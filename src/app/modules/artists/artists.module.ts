import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtistListComponent } from './artist-list/artist-list.component';
import { ArtistsListComponent } from './pages/artists-list/artists-list.component';
import { ArtistDetailComponent } from './pages/artists-list/artist-detail/artist-detail.component';



@NgModule({
  declarations: [
    ArtistListComponent,
    ArtistsListComponent,
    ArtistDetailComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ArtistsModule { }
