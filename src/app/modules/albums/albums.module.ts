import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumsListComponent } from './albums-list/albums-list.component';
import { AlbumDetailComponent } from './albums-list/album-detail/album-detail.component';



@NgModule({
  declarations: [
    AlbumsListComponent,
    AlbumDetailComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AlbumsModule { }
